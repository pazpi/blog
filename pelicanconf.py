#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Davide Pasetto'
SITENAME = 'Just my 2¢'
SITEURL = 'https://pazpi.top'
SITETITLE = AUTHOR
SITESUBTITLE = 'Engineer & geek by passion'
SITELOGO = '/images/blog-cover.jpg'
FAVICON = '/images/favicon.png'
PYGMENTS_STYLE = 'emacs'

PATH = 'content'

ARTICLE_PATHS = ['articles']

STATIC_PATH = ['articles', 'images', 'pages', 'code', 'pdf']

TIMEZONE = 'Europe/Rome'
I18N_TEMPLATES_LANG = 'en'
DEFAULT_LANG = 'en'
OG_LOCALE = 'en_US'
LOCALE = 'en_US'

DATE_FORMATS = {
    'en': '%B %d, %Y',
}

THEME = './pelican-themes/Flex'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

MAIN_MENU = True

# Blogroll
LINKS = (('Curriculum Vitae', '/cv-Pasetto-Davide.pdf'),)

MENU_ITEMS = (('Archives', '/archives.html'),
              ('Categories', '/categories.html'),
              ('Tags', '/tags.html'),)

#  LINKS = (('About', 'http://getpelican.com/'),
#  ('Python.org', 'http://python.org/'),
#  ('Jinja2', 'http://jinja.pocoo.org/'),
#  ('You can modify those links in your config file', '#'),)


# Social widget
SOCIAL = (
    ('telegram', 'https://t.me/pazpi'),
    ('gitlab', 'https://gitlab.com/pazpi'),
    ('github', 'https://github.com/pazpi'),
    ('linkedin', 'https://www.linkedin.com/in/davide-pasetto-415508119'),
    ('facebook', 'https://www.facebook.com/pasettodavide'),
    ('twitter', 'https://twitter.com/pasettodavide'),
    ('rss', '//pazpi.top/feeds/all.atom.xml')
)

CC_LICENSE = {
    'name': 'Creative Commons Attribution-ShareAlike',
    'version': '4.0',
    'slug': 'by-sa'
}

PLUGIN_PATHS = ['./pelican-plugins']
PLUGINS = ['post_stats',
           'render_math',
           'liquid_tags.img',
           'liquid_tags.include_code']

DEFAULT_PAGINATION = 10

DELETE_OUTPUT_DIRECTORY = False

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
