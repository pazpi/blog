Title: Notes - Netgear JGS524
Date: 2019-02-26
Author: pazpi

## Switch ethernet Netgear JGS524

### Problem

* the fan of this switch are too loud, impossible to use in my bedroom

### Solution 1

Remove the fan all together, and monitor the temperature and the performance

This is easier tell than done, the first try involved the esp8266 and the BMP180 which is a I2C temperature (and pressure) sensor.
For the monitoring of the data the best solution is to use the already present Grafana interface and use InfluxDB for storing the temporal data.

The main problem, never addressed, is the fact that after some hours the esp8266 inexplicably disconnect from network and the only way to re-establish the connection was to reboot the device.

In reality this solution is not a good idea, since the processor would have been constantly at around 80°C.

### Solution 2

Buy some low noise fan, best candidate the Noctua [NF-A4x10 5V](Noctua NF-A4X10 5V) which cost around 15€ on amazon.it.

The total number of fan needed is 2, for a total cost of

| Part | Single cost | Total |
| ------|-------------|-------|
| NF-A4x10 5V|12,90€  | 25,80€|

The installation is very simple, there are only two things to check but only upon arrival is possible to determine how to proceed.
Check if the noise reduction support fit into the two holes where the screw will go and if the adapter from 3-pin to 2-pin is needed.
