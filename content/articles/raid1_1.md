Title: Notes - Create RAID1 inside KRZO 
Date: 2019-02-26
Author: pazpi

# Modify the architecture of two disk inside KRZO

## Current situation

Inside KRZO there are some disk in a very messy situation, the disk containing
the root partition is a 750GB disk with also the /home partition inside.
This disk, under the $HOME directory, contains the folder `volumes`, used for
all the volumes of Docker. In this folder there is the blog root directory, the
non-external folder of Nextcloud, all the config file for different container,
ecc. In short, all the file beside the data which are on the two other main
disk.

### Data disk

Data is spread in two parts, the first on a 4TB single partition which is a single 
LVM logical volume.
This disk was used since the beginning as a pool of data, with all the file
divided into the right subdirectory (film->Film, music->Music, ecc).

The second data disk is a RAID-1 in btrfs composed of two identical disk of 2TB
from Seagate.
The primary intention of this RAID is to have some basic redundancy for at least
the most important data.

## The complication

With service like rTorrent there is a certain hassle in having multiple separate
disk attached to a container, specially if we want to maintain a clean and
intuitive director structure.
Service like Plex can be very helpful with media spread on multiple disk, but
nevertheless this is not ideal since is a very high abstraction and especially
on a local LAN Plex can be just a waste of resources and even uncomfortable to
use.

The solution of all this is to combine the disk just above the filesystem layer
using tools like MergerFS and UnionFS.

### MergerFS

For the final setup MergerFS has been used, this is perfect for the simple task
that is needed but a big overhead is introduced.

Is still unclear why this is happening but the iowait of the system is very
high, in the order of seconds sometimes and this lead to a very unstable system.
This overhead maybe is caused by the different filesystem that we want to
combine, in primis the use of a RAID-1 btrfs partition which after some test is
in fact the bottleneck of the system.

## The solution

Since there are already a LVM pool the solution is to use a RAID-1 partition
created with `mdadm` (es. /dev/rd0) and add this virtual disk to the same
physical pool but create a new virtual group and later a single 2TB logical
volume.

This will resolve the bottleneck introduced by MergerFS and still maintain
a redundant disk for critical data.

If we want to expand the main pool of data we can add multiple disk to the
logical volume which, currently, holds just a single 4TB disk.

## How to

_Nomenclature_
* First disk `sdX`
* Second disk `sdY`
* Partition number `N`

### Create RAID 1 with two disk

Es: `/dev/sdXN` is the partition N for disk 1

note: my system runs openSuse Leap 15, so keep in mind that somethings can be
different, I find that the default path for a virtual raid disk is on `/dev/md`
where on other system is just under `/dev`

Run all command as root

```
mdadm --create --verbose /dev/md/raid1_1 --level=1 --raid-devices=2 /dev/sdX /dev/sdY

mkfs.ext4 -F /dev/md/raid1_1
```

Wait till the end of the rebuilding of disk.
You can keep minitoring the state by using the `watch` command.

This step could take up to 3 hours to complete, so sit down, relax and do
somethings else.

```
watch -n 10 cat /proc/mdstat
```

Add entry in `/etc/fstab`

```
/dev/md/raid1_1 /mnt/raid1_1/	ext4	defaults	0 0
```

### [Optional] Add RAID disk as a new LVM logic volume

Create a physical device on the new disk

```
pvcreate /dev/md/raid1_1
```

Add physical disk to a new _VolumeGroup_

```
vgcreate VolGroupRaid1_1 /dev/md/raid1_1
```

And create a _logical volume_ on top

```
lvcreate -L 100%FREE VolGroupRaid1_1 -n lvolraid1_1
```

If everything is successfully with `lvdisplay` we should see the already present
logical volume __lvoldata__ and the new __lvolraid1_1__
