Title: Contacts
Date: 2018-01-21
Slug: contacts

<!-- ## Contacts -->

If you want to keep in touch write me an email at the following address: [pasettodavide@gmail.com](mailto:pasettodavide@gmail.com) or through [Telegram](https://t.me/pazpi).

I don't check so often the other social media (like Facebook).
